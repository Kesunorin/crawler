using System.Diagnostics;
using System.Net;
using MX.Core.Crypto;
using MX.NetworkProtocol;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;

namespace Utils.Rest
{
    public enum Server
    {
        JP,
        TW
    }

    public class API(Server server)
    {
        readonly Server server = server;
        readonly RestClient client = server switch
        {
            Server.JP => new("https://prod-game.bluearchiveyostar.com:5000/"),
            Server.TW => new("https://nxm-tw-bagl.nexon.com:5000/"),
            _ => throw new UnreachableException()
        };

        public string Post<T>(T packet)
            where T : RequestPacket
        {
            string json = JsonConvert.SerializeObject(packet);
            byte[] mx = PacketCryptManager.Instance.RequestToBinary(packet.Protocol, json);

            var request = new RestRequest("api/gateway", Method.Post);
            request.AddHeader("mx", "1");
            if (server == Server.JP)
                request.AddHeader("Bundle-Version", "J9Cqh6Ft62");
            request.AddFile("mx", mx, "mx.dat");

            RestResponse response;
            int retryAtMost = 5;
            for (int i = 0; i < retryAtMost; i++)
            {
                if (i > 0)
                {
                    Thread.Sleep(2000); // sleep 2 seconds
                    Console.WriteLine($"Retry for the {i + 1}th time...");
                }
                try
                {
                    response = client.Execute(request);
                    if (response.StatusCode != HttpStatusCode.OK)
                    {
                        Console.WriteLine($"Status code: {response.StatusCode}");
                        continue;
                    }
                    if (string.IsNullOrWhiteSpace(response.Content))
                    {
                        Console.WriteLine("Content is empty.");
                        continue;
                    }
                    dynamic? obj = JsonConvert.DeserializeObject<JObject>(response.Content);
                    if (obj == null)
                    {
                        Console.WriteLine("Content is not an object.");
                        continue;
                    }
                    var str = obj.packet.Value as string;
                    if (string.IsNullOrWhiteSpace(str))
                    {
                        Console.WriteLine("Response packet is empty.");
                        continue;
                    }
                    var error = JsonConvert.DeserializeObject<ErrorPacket>(str);
                    if ((error?.ErrorCode ?? WebAPIErrorCode.None) != WebAPIErrorCode.None)
                    {
                        i = 99999;
                        throw new Exception($"Error: {error!.ErrorCode}.\n{error.Reason}");
                    }
                    return str;
                }
                catch (Exception e)
                {
                    if (i >= retryAtMost - 1)
                        throw;
                    Console.WriteLine($"Error: {e.Message}");
                    continue;
                }
            }
            throw new UnreachableException();
        }
    }
}
