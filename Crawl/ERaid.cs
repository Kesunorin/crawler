using FlatData;
using MX.GameLogic.DBModel;
using MX.NetworkProtocol;
using Newtonsoft.Json;

namespace Crawl
{
    public static class ERaid
    {
        public static void SaveRankList(
            int rankFrom,
            int rankTo,
            string outputFolder = "ERaid/Rank"
        )
        {
            string outFolder = Path.Combine(Settings.saveFolder, outputFolder);
            Directory.CreateDirectory(outFolder);
            int rank = int.Max(16, rankFrom + 15);
            int stop = int.Max(rank + 1, rankTo + 15);
            for (; rank < stop; rank += 30)
            {
                EliminateRaidOpponentListResponse res = RankQuery(rank);
                var responseFilePath = Path.Combine(
                    outFolder,
                    $"{rank - 15:D8}_{rank + 14:D8}.json"
                );
                string str = JsonConvert.SerializeObject(res);
                File.WriteAllText(responseFilePath, str);
                Thread.Sleep(800);
            }
            Console.WriteLine($"Files saved to {outFolder}");
        }

        public static EliminateRaidOpponentListResponse RankQuery(int rank)
        {
            EliminateRaidOpponentListRequest packet =
                new()
                {
                    IsFirstRequest = true,
                    SearchType = RankingSearchType.Rank,
                    ClientUpTime = 12,
                    SessionKey = Settings.session,
                    Rank = rank
                };
            string res = Settings.api.Post(packet);
            var obj = JsonConvert.DeserializeObject<EliminateRaidOpponentListResponse>(res);
            ArgumentNullException.ThrowIfNull(obj);
            return obj;
        }

        /// <remarks>
        /// Score reference: <br/>
        /// 4 min: TM=40348000 INS=27928000 EX=15344000 <br/>
        /// 3 min: TM=39716000 INS=26161600 EX=14576000 <br/>
        /// </remarks>
        public static void SaveScoreList(
            ArmorType armor,
            long scoreFrom,
            long scoreTo,
            string outputFolder = "ERaid"
        )
        {
            string outFolder = Path.Combine(Settings.saveFolder, outputFolder);
            Directory.CreateDirectory(outFolder);

            EliminateRaidOpponentListRequest packet =
                new()
                {
                    BossGroupIndex = (int)armor,
                    ClientUpTime = 10,
                    SearchType = RankingSearchType.Score,
                    SessionKey = Settings.session
                };

            long score = scoreFrom;
            int retry = 0;
            while (score > scoreTo)
            {
                packet.Score = score;
                string res = Settings.api.Post(packet);
                var obj = JsonConvert.DeserializeObject<EliminateRaidOpponentListResponse>(res);
                ArgumentNullException.ThrowIfNull(obj);
                long id = obj.OpponentUserDBs.LastOrDefault()?.AccountId ?? 0;
                if (id == 0)
                {
                    if (++retry > 3)
                    {
                        break;
                    }
                }
                else
                {
                    long lastScore = GetScore(obj.OpponentUserDBs.Last(), armor);
                    var responseFilePath = Path.Combine(
                        outFolder,
                        $"score_{score:D8}_{lastScore:D8}.json"
                    );
                    File.WriteAllText(responseFilePath, res);
                    score = lastScore;
                    retry = 0;
                }
                Thread.Sleep(1000);
            }
            Console.WriteLine($"Files saved to {outFolder}");
        }

        static long GetScore(EliminateRaidUserDB user, ArmorType armor)
        {
            return user
                .BossGroupToRankingPoint.Where(kvp => kvp.Key.EndsWith(armor.ToString()))
                .Select(kvp => kvp.Value)
                .SingleOrDefault(0);
        }

        public static long[] LoadScoreList(
            string folderName,
            ArmorType armor,
            long scoreFrom,
            long scoreTo
        )
        {
            Dictionary<long, long> dict = [];

            string folderPath = Path.Combine(Settings.saveFolder, folderName);
            string[] files = Directory.GetFiles(folderPath);
            var sortedFiles = files.OrderByDescending(f => f).ToArray();
            foreach (string file in sortedFiles)
            {
                string json = File.ReadAllText(file);
                var obj = JsonConvert.DeserializeObject<EliminateRaidOpponentListResponse>(json);
                if (obj == null)
                    continue;
                foreach (var user in obj.OpponentUserDBs)
                {
                    var id = user.AccountId;
                    var score = GetScore(user, armor);
                    if (score > scoreFrom || score < scoreTo)
                        continue;
                    dict.TryAdd(id, score);
                }
            }
            return dict.OrderByDescending(kvp => kvp.Value).Select(kvp => kvp.Key).ToArray();
        }

        public static void SaveBestTeam(long[] accountIds, string outputFolder = "ERaid/Team")
        {
            string outFolder = Path.Combine(Settings.saveFolder, outputFolder);
            Directory.CreateDirectory(outFolder);

            EliminateRaidGetBestTeamRequest packet =
                new() { ClientUpTime = 4, SessionKey = Settings.session };

            foreach (var id in accountIds)
            {
                packet.SearchAccountId = id;
                string res = Settings.api.Post(packet);
                var obj = JsonConvert.DeserializeObject<EliminateRaidGetBestTeamResponse>(res);
                ArgumentNullException.ThrowIfNull(obj);
                var responseFilePath = Path.Combine(outFolder, $"id_{id:D9}.json");
                File.WriteAllText(responseFilePath, res);
                Thread.Sleep(1000);
            }
            Console.WriteLine($"Files saved to {outFolder}");
        }
    }
}
