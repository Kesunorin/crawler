using System.Diagnostics;
using MX.NetworkProtocol;
using Newtonsoft.Json;

namespace Crawl
{
    public static class RaidTier
    {
        private class Rankings
        {
            public int PlatinumRank { get; set; }
            public int GoldRank { get; set; }
            public int SilverRank { get; set; }
        }

        //读取配置文件确认档线
        private static Rankings GetRankingsFromFile()
        {
            string folder = Path.Combine(AppContext.BaseDirectory, "TierConfig");
            string serverString = Environment.GetEnvironmentVariable("Server") ?? "";
            string jsonfile = serverString switch
            {
                "JP" => "JPRankings.json",
                "TW" => "TWRankings.json",
                _ => throw new UnreachableException()
            };
            string file = Path.Combine(folder, jsonfile);
            string filecontent = File.ReadAllText(file);
            var rankings = JsonConvert.DeserializeObject<Rankings>(filecontent);
            ArgumentNullException.ThrowIfNull(rankings);
            return rankings;
        }

        //主办法，爬取档线和难度人数
        public static void TierAndDifficultyPeople(List<long>? customDiffScore = null)
        {
            RaidLobby raidLobby = new();
            RaidLobbyResponse RaidLobbyRes = raidLobby.RaidLobbyQuery();
            string bossName = RaidLobbyRes.RaidLobbyInfoDB.PlayableHighestDifficulty.Keys.First();
            int raidTime =
                (bossName.StartsWith("Binah") || bossName.StartsWith("Kaitenger")) ? 3 : 4;
            Rankings rankings = GetRankingsFromFile();
            var TierRank = FetchTierRank(
                rankings.PlatinumRank,
                rankings.GoldRank,
                rankings.SilverRank
            );
            var difficultyPeople = FetchDifficultyPeople(raidTime, customDiffScore);
            var result = new { Rankings = TierRank, DifficultyPeople = difficultyPeople, };
            string res = JsonConvert.SerializeObject(result);
            SaveJSON(res);
        }

        //保存json和转换后的txt
        private static void SaveJSON(string res, string outputFolder = "TierAndDifficultyPeople")
        {
            string outFolder = Path.Combine(Settings.saveFolder, outputFolder);
            Directory.CreateDirectory(outFolder);
            var JSONFilePath = Path.Combine(outFolder, $"TierAndDifficultyPeople.json");
            File.WriteAllText(JSONFilePath, res);
            var TXTFilePath = Path.Combine(outFolder, $"TierAndDifficultyPeople.txt");
            var data = JsonConvert.DeserializeObject<Dictionary<string, Dictionary<string, long>>>(
                res
            );
            ArgumentNullException.ThrowIfNull(data);
            List<string> outputLines = [];
            foreach (var ranking in data["Rankings"])
            {
                string rankNum = ranking.Key.Replace("Rank", "");
                outputLines.Add($"第{rankNum}名:{ranking.Value}");
            }

            foreach (var difficulty in data["DifficultyPeople"])
            {
                outputLines.Add($"{difficulty.Key}:{difficulty.Value}");
            }
            File.WriteAllLines(TXTFilePath, outputLines);
            Console.WriteLine($"Files saved to {outFolder}");
        }

        //确认档线的分数
        private static Dictionary<string, long> FetchTierRank(
            int platinumRank,
            int goldRank,
            int silverRank
        )
        {
            var rankings = new Dictionary<string, long>();
            int rank = 1;
            while (rank <= platinumRank)
            {
                rankings[$"Rank{rank}"] = Raid.FetchBestRankingPoint(rank);
                Thread.Sleep(1000);
                rank = rank == 1 ? 1000 : rank + 1000;
            }
            rankings[$"Rank{goldRank}"] = Raid.FetchBestRankingPoint(goldRank);
            Thread.Sleep(1000);
            rankings[$"Rank{silverRank}"] = Raid.FetchBestRankingPoint(silverRank);
            Thread.Sleep(1000);
            return rankings;
        }

        //返回各难度的人数
        private static Dictionary<string, long> FetchDifficultyPeople(
            int raidTime,
            List<long>? customDiffScore
        )
        {
            List<long> defaultDiffScore =
                raidTime == 3
                    ? [26161600, 14576000, 7288000, 3644000, 1822000, 911000]
                    : [27928000, 15344000, 7672000, 3836000, 1918000, 959000];
            List<long> diffScore = customDiffScore ?? defaultDiffScore;
            var difficultyPeople = new Dictionary<string, long>();
            var labels = new List<string>
            {
                "Torment",
                "Insane",
                "Extreme",
                "HardCore",
                "VeryHard",
                "Hard",
                "Normal"
            };
            long previousRank = 0;
            int i = 0;
            for (; i < diffScore.Count; i++)
            {
                long score = diffScore[i];
                long currentRank = Raid.FetchRank(score) - 1;
                Thread.Sleep(1000);
                difficultyPeople[labels[i]] = currentRank - previousRank;
                previousRank = currentRank;
            }
            difficultyPeople[labels[i]] = FetchLastRank(previousRank) - previousRank + 1;
            return difficultyPeople;
        }

        //寻找最后一名，用于判断normal难度人数
        private static long FetchLastRank(long previousRank)
        {
            long left = previousRank;
            long right = previousRank;
            int index = 1;
            while (true)
            {
                right += index;
                index *= 2;
                Thread.Sleep(1000);
                if (Raid.FetchBestRankingPoint(right) == 0)
                    break;
                left = right;
            }
            long ans = left;
            right -= 1;
            while (left <= right)
            {
                long mid = (left + right) / 2;
                if (Raid.FetchBestRankingPoint(mid) == 0)
                    right = mid - 1;
                else
                {
                    left = mid + 1;
                    ans = mid;
                }
                Thread.Sleep(1000);
            }
            return ans;
        }
    }
}
